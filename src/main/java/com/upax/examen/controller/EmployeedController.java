package com.upax.examen.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upax.examen.component.EmployeedComponent;
import com.upax.examen.model.Employee;
import com.upax.examen.value.EmployeedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path = "/employeed")
public class EmployeedController {

    private final EmployeedComponent employeedComponent;

    @Autowired
    public EmployeedController(EmployeedComponent employeedComponent) {
        this.employeedComponent = employeedComponent;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> saveEmployeed(@RequestBody EmployeedValue employeedValue) {
        return ResponseEntity.ok(employeedComponent.saveEmployeed(employeedValue));
    }

    @RequestMapping(value = "findAll/{idPuesto}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> findAllEmployeed(@PathVariable int idPuesto) {
        return ResponseEntity.ok(employeedComponent.findAllEmployees(idPuesto));
    }

    @GetMapping(value = "/getemployees")
    public ResponseEntity<List<Employee>> getEmployees() {
        return ResponseEntity.ok(employeedComponent.getEmployees());
    }

    @GetMapping(value = "/getemployees/flux")
    public ResponseEntity<List<Employee>> getEmployeeFlux() {
        return ResponseEntity.ok(employeedComponent.getEmployeeFlux());
    }
}
