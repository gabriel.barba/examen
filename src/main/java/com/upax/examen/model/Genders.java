/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upax.examen.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author A.Selena
 */
@Entity
@Table(name = "genders", catalog = "examen_tecnico", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Genders.findAll", query = "SELECT g FROM Genders g"),
    @NamedQuery(name = "Genders.findByIdGenders", query = "SELECT g FROM Genders g WHERE g.idGenders = :idGenders"),
    @NamedQuery(name = "Genders.findByName", query = "SELECT g FROM Genders g WHERE g.name = :name")})
public class Genders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_genders", nullable = false)
    private Integer idGenders;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 45)
    private String name;
    @OneToMany(mappedBy = "gendersId")
    private List<Employees> employeesList;

    public Genders() {
    }

    public Genders(Integer idGenders) {
        this.idGenders = idGenders;
    }

    public Genders(Integer idGenders, String name) {
        this.idGenders = idGenders;
        this.name = name;
    }

    public Integer getIdGenders() {
        return idGenders;
    }

    public void setIdGenders(Integer idGenders) {
        this.idGenders = idGenders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Employees> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(List<Employees> employeesList) {
        this.employeesList = employeesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGenders != null ? idGenders.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Genders)) {
            return false;
        }
        Genders other = (Genders) object;
        if ((this.idGenders == null && other.idGenders != null) || (this.idGenders != null && !this.idGenders.equals(other.idGenders))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.upax.examen.model.Genders[ idGenders=" + idGenders + " ]";
    }
    
}
