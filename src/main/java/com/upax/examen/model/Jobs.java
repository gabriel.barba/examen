/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upax.examen.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "jobs")
public class Jobs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_jobs", nullable = false)
    @Getter
    @Setter
    private Integer idJobs;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 255)
    @Getter
    @Setter
    private String name;
    @Basic(optional = false)
    @Column(name = "salary", nullable = false)
    @Getter
    @Setter
    private double salary;
    @OneToMany(mappedBy = "jobId")
    @Getter
    @Setter
    private List<Employees> employeesList;

    public Jobs() {
    }

    public Jobs(Integer idJobs) {
        this.idJobs = idJobs;
    }

    public Jobs(Integer idJobs, String name, double salary) {
        this.idJobs = idJobs;
        this.name = name;
        this.salary = salary;
    }

    public static final class Builder {
        private Integer idJobs;
        private String name;
        private double salary;
        private List<Employees> employeesList;
    }
}
