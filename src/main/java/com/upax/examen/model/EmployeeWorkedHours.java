/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upax.examen.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author A.Selena
 */
@Entity
@Table(name = "employee_worked_hours", catalog = "examen_tecnico", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeeWorkedHours.findAll", query = "SELECT e FROM EmployeeWorkedHours e"),
    @NamedQuery(name = "EmployeeWorkedHours.findByIdEmployeeWorkedHours", query = "SELECT e FROM EmployeeWorkedHours e WHERE e.idEmployeeWorkedHours = :idEmployeeWorkedHours"),
    @NamedQuery(name = "EmployeeWorkedHours.findByWorkedHours", query = "SELECT e FROM EmployeeWorkedHours e WHERE e.workedHours = :workedHours"),
    @NamedQuery(name = "EmployeeWorkedHours.findByWorkedDate", query = "SELECT e FROM EmployeeWorkedHours e WHERE e.workedDate = :workedDate")})
public class EmployeeWorkedHours implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_employee_worked_hours", nullable = false)
    private Integer idEmployeeWorkedHours;
    @Column(name = "worked_hours")
    private Integer workedHours;
    @Column(name = "worked_date")
    @Temporal(TemporalType.DATE)
    private Date workedDate;
    @JoinColumn(name = "employee_id", referencedColumnName = "id_employees")
    @ManyToOne
    private Employees employeeId;

    public EmployeeWorkedHours() {
    }

    public EmployeeWorkedHours(Integer idEmployeeWorkedHours) {
        this.idEmployeeWorkedHours = idEmployeeWorkedHours;
    }

    public Integer getIdEmployeeWorkedHours() {
        return idEmployeeWorkedHours;
    }

    public void setIdEmployeeWorkedHours(Integer idEmployeeWorkedHours) {
        this.idEmployeeWorkedHours = idEmployeeWorkedHours;
    }

    public Integer getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(Integer workedHours) {
        this.workedHours = workedHours;
    }

    public Date getWorkedDate() {
        return workedDate;
    }

    public void setWorkedDate(Date workedDate) {
        this.workedDate = workedDate;
    }

    public Employees getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employees employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmployeeWorkedHours != null ? idEmployeeWorkedHours.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeWorkedHours)) {
            return false;
        }
        EmployeeWorkedHours other = (EmployeeWorkedHours) object;
        if ((this.idEmployeeWorkedHours == null && other.idEmployeeWorkedHours != null) || (this.idEmployeeWorkedHours != null && !this.idEmployeeWorkedHours.equals(other.idEmployeeWorkedHours))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.upax.examen.model.EmployeeWorkedHours[ idEmployeeWorkedHours=" + idEmployeeWorkedHours + " ]";
    }
    
}
