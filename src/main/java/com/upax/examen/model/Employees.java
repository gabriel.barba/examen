/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upax.examen.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "employees")
public class Employees implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_employees", nullable = false)
    @Getter
    @Setter
    private Integer idEmployees;
    @Column(name = "name", length = 255)
    @Getter
    @Setter
    private String name;
    @Column(name = "last_name", length = 255)
    @Getter
    @Setter
    private String lastName;
    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    @Getter
    @Setter
    private Date birthdate;
    @OneToMany(mappedBy = "employeeId")
    @Getter
    @Setter
    private List<EmployeeWorkedHours> employeeWorkedHoursList;
    @JoinColumn(name = "genders_id", referencedColumnName = "id_genders")
    @ManyToOne
    @Getter
    @Setter
    private Genders gendersId;
    @JoinColumn(name = "job_id", referencedColumnName = "id_jobs")
    @ManyToOne
    @Getter
    @Setter
    private Jobs jobId;

    public Employees() {
    }

    public Employees(Integer idEmployees) {
        this.idEmployees = idEmployees;
    }

    public static final class Builder {
        private Integer idEmployees;
        private String name;
        private String lastName;
        private Date birthdate;
        private Genders gendersId;
        private Jobs jobId;

        private Builder() {}

        public static Builder anEmployees() {
            return new Builder();
        }

        public Builder idEmployees(Integer idEmployees) {
            this.idEmployees = idEmployees;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder birthdate(Date birthdate) {
            this.birthdate = birthdate;
            return this;
        }

        public Builder gendersId(Genders gendersId) {
            this.gendersId = gendersId;
            return this;
        }

        public Builder jobId(Jobs jobId) {
            this.jobId = jobId;
            return this;
        }

        public Employees build() {
            Employees employees = new Employees();
            employees.setIdEmployees(idEmployees);
            employees.setName(name);
            employees.setLastName(lastName);
            employees.setBirthdate(birthdate);
            employees.setGendersId(gendersId);
            employees.setJobId(jobId);
            return employees;
        }
    }
}
