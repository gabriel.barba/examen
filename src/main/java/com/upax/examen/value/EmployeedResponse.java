package com.upax.examen.value;

import lombok.Getter;
import lombok.Setter;

public class EmployeedResponse {
    @Getter @Setter
    private Integer id;
    @Getter @Setter
    private boolean success;
}
