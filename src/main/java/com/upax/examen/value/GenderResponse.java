package com.upax.examen.value;

import lombok.Getter;
import lombok.Setter;

public class GenderResponse {
    @Getter
    @Setter
    private Integer id;
    @Getter @Setter
    private String name;
}
