package com.upax.examen.value;

import lombok.Getter;
import lombok.Setter;

public class JobResponse {
    @Getter @Setter
    private Integer id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private double salary;
}
