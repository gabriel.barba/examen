package com.upax.examen.value;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class EmployeedListResponse {
    @Getter
    @Setter
    private Integer id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private Date birthdate;
    @Getter @Setter
    private JobResponse jobResponse;
    @Getter @Setter
    private GenderResponse genderResponse;
}
