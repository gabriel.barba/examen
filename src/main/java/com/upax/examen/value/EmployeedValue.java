package com.upax.examen.value;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class EmployeedValue {
    @Setter @Getter
    private int gender_id;
    @Setter @Getter
    private int job_id;
    @Setter @Getter
    private String name;
    @Setter @Getter
    private String last_name;
    @Setter @Getter
    private Date birthdate;
}
