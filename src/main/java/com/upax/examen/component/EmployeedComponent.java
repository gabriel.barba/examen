package com.upax.examen.component;

import com.upax.examen.model.Employee;
import com.upax.examen.model.Employees;
import com.upax.examen.value.EmployeedListResponse;
import com.upax.examen.value.EmployeedResponse;
import com.upax.examen.value.EmployeedValue;

import java.util.List;

public interface EmployeedComponent {
    EmployeedResponse saveEmployeed(EmployeedValue employeedValue);
    List<EmployeedListResponse> findAllEmployees(int idPuesto);
    List<Employee> getEmployees();
    List<Employee> getEmployeeFlux();
}
