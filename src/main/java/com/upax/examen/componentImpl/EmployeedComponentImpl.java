package com.upax.examen.componentImpl;

import com.upax.examen.component.EmployeedComponent;
import com.upax.examen.exceptions.EmployeedException;
import com.upax.examen.model.Employee;
import com.upax.examen.model.Employees;
import com.upax.examen.model.Genders;
import com.upax.examen.model.Jobs;
import com.upax.examen.repository.EmployeesRepository;
import com.upax.examen.repository.GendersRepository;
import com.upax.examen.repository.JobRepository;
import com.upax.examen.value.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class EmployeedComponentImpl implements EmployeedComponent {
    private final JobRepository jobRepository;
    private final GendersRepository gendersRepository;
    private final EmployeesRepository employeesRepository;

    @Autowired
    public EmployeedComponentImpl(JobRepository jobRepository, GendersRepository gendersRepository, EmployeesRepository employeesRepository) {
        this.jobRepository = jobRepository;
        this.gendersRepository = gendersRepository;
        this.employeesRepository = employeesRepository;
    }

    @Override
    public EmployeedResponse saveEmployeed(EmployeedValue employeedValue) {
        Employees result = employeesRepository.findEmployeedByNameAndLastName(employeedValue.getName(), employeedValue.getLast_name());
        EmployeedResponse employeedResponse = new EmployeedResponse();
        if (result == null) {
            if (calculateYear(employeedValue.getBirthdate())) {
                employeedResponse.setId(null);
                employeedResponse.setSuccess(false);
                return employeedResponse;
            } else {
                Jobs jobs = jobRepository.findByIdJobs(employeedValue.getJob_id());
                if (jobs == null) {
                    employeedResponse.setId(null);
                    employeedResponse.setSuccess(false);
                    return employeedResponse;
                } else {
                    Genders genders = gendersRepository.findByIdGenders(employeedValue.getGender_id());
                    Employees employees = Employees.Builder.anEmployees().name(employeedValue.getName()).lastName(employeedValue.getLast_name()).birthdate(employeedValue.getBirthdate()).jobId(jobs).gendersId(genders).build();
                    employeesRepository.save(employees);
                    employeedResponse.setId(employees.getIdEmployees());
                    employeedResponse.setSuccess(true);
                    return employeedResponse;
                }
            }
        } else {
            employeedResponse.setId(null);
            employeedResponse.setSuccess(false);
            return employeedResponse;
        }
    }

    @Override
    public List<EmployeedListResponse> findAllEmployees(int idPuesto) {
        List<Employees> employeesList = employeesRepository.findAll();
        List<Employees> employeesFilter = employeesList.stream().filter(c -> c.getJobId().getIdJobs() == idPuesto).collect(Collectors.toList());
        List<Employees> employeesOrder = employeesFilter.stream().sorted(Comparator.comparing(Employees::getLastName)).collect(Collectors.toList());

        List<EmployeedListResponse> employeedResponses = new ArrayList<>();
        for (Employees e:employeesOrder) {
            EmployeedListResponse er = new EmployeedListResponse();
            er.setId(e.getIdEmployees());
            er.setName(e.getName());
            er.setBirthdate(e.getBirthdate());
            JobResponse jr = new JobResponse();
            jr.setId(e.getJobId().getIdJobs());
            jr.setName(e.getJobId().getName());
            jr.setSalary(e.getJobId().getSalary());
            er.setJobResponse(jr);
            GenderResponse gr = new GenderResponse();
            gr.setId(e.getGendersId().getIdGenders());
            gr.setName(e.getGendersId().getName());
            er.setGenderResponse(gr);
            employeedResponses.add(er);
        }
        return employeedResponses;
    }

    @Override
    public List<Employee> getEmployees() {
        RestTemplate restTemplate = new RestTemplate();
        String employeeUrl = "http://localhost:8081/employees";
        ResponseEntity<Employee[]> employees = restTemplate.getForEntity(employeeUrl, Employee[].class);
        List<Employee> ea = Arrays.asList(employees.getBody());
        for (Employee e: ea) {
            System.out.println("Employee: " + e.getName());
        }

        return ea;
    }

    @Override
    public List<Employee> getEmployeeFlux() {
        WebClient client = WebClient.create("http://localhost:8081");
        Flux<Employee> employeeFlux = client.get().uri("/employees").retrieve().bodyToFlux(Employee.class);
        employeeFlux.subscribe(System.out::println);
        return employeeFlux.buffer().blockFirst();
    }

    public static boolean calculateYear(Date dateBirth) {
        Date dateNow = new Date();

        Calendar calNow = Calendar.getInstance(Locale.US);
        calNow.setTime(dateNow);

        Calendar calBirth = Calendar.getInstance(Locale.US);
        calNow.setTime(dateBirth);

        return (calBirth.get(Calendar.YEAR) - calNow.get(Calendar.YEAR)) < 18;
    }
}
