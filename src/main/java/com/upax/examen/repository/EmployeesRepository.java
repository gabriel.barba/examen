package com.upax.examen.repository;

import com.upax.examen.model.Employees;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeesRepository extends JpaRepository<Employees, Integer> {
    Employees findEmployeedByNameAndLastName(String name, String lastName);
}
