package com.upax.examen.repository;

import com.upax.examen.model.Genders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GendersRepository extends JpaRepository<Genders, Integer> {
    Genders findByIdGenders(int idGenders);
}
