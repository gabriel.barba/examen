package com.upax.examen.repository;

import com.upax.examen.model.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Jobs, Integer> {
    Jobs findByIdJobs(int idJobs);
}
