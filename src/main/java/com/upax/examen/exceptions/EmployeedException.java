package com.upax.examen.exceptions;

public class EmployeedException extends RuntimeException {

    private EmployeedException(String message) {
        super(message);
    }

    public static EmployeedException of(String value) {
        return new EmployeedException(value);
    }
}
